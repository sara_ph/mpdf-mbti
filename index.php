<?php
// $index = file_get_contents('http://hr.milogy.com/index.php');
$index = file_get_contents("./files/index.php");
$style = file_get_contents('./files/style.css');

try {
    require_once __DIR__ . '/vendor/autoload.php';
    $mpdf = new \Mpdf\Mpdf(["default_font" => "iran_sans1", 'mode' => 'utf-8']);
    $mpdf->setBasePath('http://book.salarihamid.ir/');
    $mpdf->WriteHTML($style, \Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($index, \Mpdf\HTMLParserMode::HTML_BODY);


    $mpdf->autoScriptToLang = true;
    $mpdf->autoLangToFont = true;
    $mpdf->SetDirectionality('rtl');
    $mpdf->Output("");
} catch (\Mpdf\MpdfException $e) {
    echo $e->getMessage();
}
